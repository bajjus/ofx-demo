# README #

Checkout Develop branch

### What is this repository for? ###

A simple Demo application using react-native

### How do I get set up? ###

Install packages

`yarn`

To run

IOS :

`react-native run-ios`

Android:

`react-native run-android`

TEST: 

`yarn test`

### TODO ###

Add tests around layouts
